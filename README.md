RemindAt
========

What for?
---------

Imagine you send an e-mail, by example to your bank. It's done, your forget it.
And you realize two months later you never receive a reply.

With RemindAt, you can do that:

    From: me@example.com
    To: mybank@babylon.com
    Bcc: +5d@remind.example.com
    Subject: Important message
    [...]

Note the Bcc field: it ask the remindat system to send you a remind e-mail in
5 days.

RemindAt is intended to be installed on a e-mail server (along with the MTA),
to provide the service to the users of this server.

What is needed?
---------------

RemindAt needs:

* Python 3
* An unix system with the `at` command (RemindAt use `at` to trigger the remind)
* a MTA

How to make it works
--------------------

(Postfix configuration)

1. create a system user (be original, call it `remindat`), and make sur to
   give the right permission to following files.

2. put the `remindat.py` script, let's say, in `/usr/local/bin`, make sure
   it's executable by the `remindat` user.

3. make a spool directory (`/var/spool/remindat`), with read and write access
   to `remindat`.

4. copy `remindat.conf.sample` into `/etc/remindat.conf`, and set the right
   values (the spool directory choosen at 3, the name of your service, and a
   brand new domain name - doesn't have to exist). 

5. copy translations files :

    ```
    rsync -avz locale /usr/local/share/
    ```

5. in the Postfix `/etc/postfix/master.cf`, add:

    ```
    remindat  unix -        n       n       -       -       pipe
      flags=FRD user=remindat argv=/usr/bin/python3 /usr/local/bin/remindat.py --localedir=/usr/local/share/locale
    ```

6. in the Postfix `/etc/postfix/main.cf`, add:

    ```
    # create a transport map (to send all mail for your service_domain 
    # to remindat)
    transport_maps = hash:/etc/postfix/transport
    remindat_destination_recipient_limit = 1
    
    # to allow only users that cand send e-mail using your MTA, the 
    # service_domain must not exists, and must not be listed in relay_domains 
    # or my_destination. If the service_domain is a subdomain of a domain listed
    # in relay_domains or my_destination, you must disable the postfix function
    # where parent domains matches subdomains. To do that, uncomment the
    # following line.
    #parent_domain_matches_subdomains = debug_peer_list,fast_flush_domains,mynetworks,permit_mx_backup_networks,qmqpd_authorized_clients,smtpd_access_maps
    ```

7. create the `/etc/postfix/transport` file:

    ```
    your.service.domain                  remindat:
    ```

8. reload the configuration (here Debian commands):

    ```
    postmap /etc/postfix/transport
    service postfix reload
    ```

9. test it! Send an e-mail to help@your.service.domain to get an e-mail with
    remind instructions.
