��          �      L      �  �  �  2   v     �  
   �     �     �     �  &   �          9     G     I     K     P     U      W     x     z  4  |  #  �	  8   �               !     :     C  $   ^  %   �     �     �     �     �     �     �  '   �     �     �                                                          	              
                            
%(name_line)s******************
%(service_domain)s -- how to use it?
%(name_line)s******************

1. Obtain a remind when sending an e-mail
=========================================

You just have to add a particular adress in hidden
copy (Bcc) of the e-mail you want to send.
By example:
    
    <16-09-21@%(service_domain)s> will send you a remind the
                21 of septembre 2016 (considering this is a
                date in the future)
    <+3j@%(service_domain)s> will send you a remind in 3 days. 
    
    You can specify a date in formats:
        yy-mm-dd ou yy-mm-ddThh:mn
        (yy : year, mm : month, dd : day, hh : hour, mn : minute)
    
    You can ggive a shift in the time with the format:
        +Nd   +Nw   +Nm   +Ny
            N is a number, d=days, w=weeks, m=months, y=year    
        +Nh    +NhMIN
            h=hours, MIN is a number of minutes
        example:
        +5d3h42 -> in 5 days, 3 hours, 42 minutes

2. Give me the list of my reminds, delete one of them
=====================================================

You just have to send an e-mail (don't mind subject and message) to: 
    
    list@%(service_domain)s

The software will answer you by e-mail the list of your reminds. 
It will give for each an adress, where you just need to send an
e-mail to delete the remind.

3. To give this help message
============================

You just need to send an e-mail to:
    
    help@%(service_domain)s

 Command <%s@%s> is not valid... here is some help. Help My reminds Remind set the %s. Remind: The remind %s does not exists. The remind %s was succesfully deleted. You have no programmed remind. Your reminds: d h help list m to delete it, send an e-mail to: w y Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2016-09-14 19:04+CEST
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Florian Birée <florian@biree.name>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: utf-8
Generated-By: pygettext.py 1.5
 
%(name_line)s************************
%(service_domain)s -- comment l'utiliser ?
%(name_line)s************************

1. Obtenir un rappel lors de l'envoi d'un e-mail
================================================

Il suffit d'ajouter une adresse particulière en copie
cachée (Bcc) du mail que vous voulez envoyer.
Par exemple :

    <16-09-21@%(service_domain)s> vous enverra un rappel le
                21 septembre 2016 (en considérant qu'il s'agit
                d'une date dans le futur)
    <+3j@%(service_domain)s> vous enverra un rappel dans 3 jours.

    Vous pouvez préciser une date au format :
        aa-mm-jj ou aa-mm-jjThh:mn
        (aa : année, mm : mois, jj : jour, hh : heure, mn : minute)

    Vous pouvez donner un décalage dans le temps au format :
        +Nj   +Ns   +Nm   +Na
            N est un nombre, j=jours, s=semmaines, m=mois, a=années
        +Nh    +NhMIN
            h=heures, MIN est un nombre de minutes
        une combinaison des précédents
        +5j3h42 -> dans cinq jours, trois heures et 42 minutes

2. Obtenir la liste de mes rappels, en supprimer un
===================================================


Il suffit d'envoyer un mail (peut importe le sujet et le message) à l'adresse :

    liste@%(service_domain)s

Le logiciel vous répondra par e-mail la liste de vos rappel. Il donnera
également pour chacun une adresse à laquelle il suffira d'écrire pour supprimer
le rappel.

3. Obtenir ce message d'aide
============================

Il suffit d'envoyer un e-mail à :

    aide@%(service_domain)s

 La commande <%s@%s> n'est pas valide... voici de l'aide. Aide Vos rappels : Rappel programmé le %s. Rappel : Le rappel %s n'existe pas. Le rappel %s a bien été supprimé. Vous n'avez pas de rappel programmé. Vos rappels : j h aide liste m pour le supprimer, envoyez un mail à : s a 