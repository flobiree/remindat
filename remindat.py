#!/usr/bin/python3
# -*- coding: utf-8 -*-
###############################################################################
#       remindat.py
#       
#       Copyright © 2016-2018, Florence Birée <flo@biree.name>
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Remind an e-mail"""

__author__ = "Florence Birée"
__version__ = "0.4"
__license__ = "GPLv3"
__copyright__ = "Copyright © 2016-2018, Florence Birée <flo@biree.name>"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import re
import sys
import uuid
import email
import email.message
import email.mime.text
import smtplib
import datetime
import argparse
from email.utils import formatdate
from subprocess import Popen, PIPE, check_call 
import configparser
import gettext

# Big strings
def _(message): return message
helpmessage = _("""\

%(name_line)s******************
%(service_domain)s -- how to use it?
%(name_line)s******************

1. Obtain a remind when sending an e-mail
=========================================

You just have to add a particular adress in hidden
copy (Bcc) of the e-mail you want to send.
By example:
    
    <16-09-21@%(service_domain)s> will send you a remind the
                21 of septembre 2016 (considering this is a
                date in the future)
    <+3j@%(service_domain)s> will send you a remind in 3 days. 
    
    You can specify a date in formats:
        yy-mm-dd ou yy-mm-ddThh:mn
        (yy : year, mm : month, dd : day, hh : hour, mn : minute)
    
    You can ggive a shift in the time with the format:
        +Nd   +Nw   +Nm   +Ny
            N is a number, d=days, w=weeks, m=months, y=year    
        +Nh    +NhMIN
            h=hours, MIN is a number of minutes
        example:
        +5d3h42 -> in 5 days, 3 hours, 42 minutes

2. Give me the list of my reminds, delete one of them
=====================================================

You just have to send an e-mail (don't mind subject and message) to: 
    
    list@%(service_domain)s

The software will answer you by e-mail the list of your reminds. 
It will give for each an adress, where you just need to send an
e-mail to delete the remind.

3. To give this help message
============================

You just need to send an e-mail to:
    
    help@%(service_domain)s

""") 
del _ 

# Logging stuff
def _print(*msg):
    print(*msg, file=sys.stderr)

# Main classes
class ReplyMessage(email.mime.text.MIMEText):
    """Build a reply to a message"""
    
    def __init__(self, msg, text, cite=True, subject_prefix='Re:', 
                 fromadress=None, newsubject=None):
        """Make the reply
            msg is the original message
            text is the reply body text
            cite is True append the original message to the reply text
            subject_prefix is the prefix added before the subject
            fromadress allow to specify a From adress different that the
                original To adress
        """
        self.orig = msg
        email.mime.text.MIMEText.__init__(self, 
            (text + (self._citeorigtext() if cite else '')).encode('utf-8'), 
            _charset='utf-8'
        )
        
        # Fix headers
        if newsubject is None:
            try:
                self["Subject"] = (
                    subject_prefix + ' ' +
                        self.orig["Subject"].\
                        replace(subject_prefix + ' ', "").\
                        replace(subject_prefix.upper() + ' ', "")
                )
            except TypeError:
                self["Subject"] = subject_prefix
        else:
            try:
                self["Subject"] = newsubject + ' (was: ' + self.orig["Subject"] + ')'
            except TypeError:
                self["Subject"] = newsubject
        self['In-Reply-To'] = self.orig["Message-ID"]
        self['References'] = self.orig["Message-ID"]#+orig["References"].strip()
        self['Thread-Topic'] = self.orig["Thread-Topic"]
        self['Thread-Index'] = self.orig["Thread-Index"]
        self['From'] = (
            self.orig["Delivered-To"] 
            if fromadress is None 
            else fromadress
        )
        self['To'] = self.orig["From"]
        self['Message-ID'] = email.utils.make_msgid()
        
        msgbody = ''
        # Cite old content
        if cite:
            msgbody = self._citeorigtext()
        
        # Add content
        msgbody = text + msgbody
    
    def _citeorigtext(self):
        """Return the text of the original message, prefixed by ">" """
        newtext = "\n"
        
        for part in self.orig.walk():
            if part.get_content_type().startswith("text/plain"):
                newtext += "\n> "
                body = part.get_payload(decode=False)
                body = body.replace("\n","\n> ")
                newtext += body
        return newtext
    
    def send(self):
        """Send the e-mail using the localhost smtp"""
        self["Date"] = formatdate(localtime=True)
        s = smtplib.SMTP('localhost')
        s.send_message(self)
        s.quit()



class AtCmd:
    """Build an AT command from an e-mail"""
    
    abstime = re.compile(r"""
        ^
        (?P<year>\d{2})                    # years
        (?:-(?P<month>\d{1,2})             # months
            (?:-(?P<days>\d{1,2})           # days
                (?:[Tt](?P<hour>\d{1,2})      # hours
                    (?::(?P<minute>\d{2}))?    # minutes
                )?
            )?
        )?
        $
    """, re.VERBOSE)
    
    atqtask = re.compile(r"""
        (?P<id>\d+)         # atq task id
        \s+                 # whitespace
        (?P<date>\w+\s+\w+\s+\d+\s+[\d:]+\s+\d+) #date
    """, re.VERBOSE)
    
    atc_remid = re.compile(r"""
        remindat\.py        # command name, maybe we should get it from argv
        \s+
        -s                  # -s parameter
        \s+
        (?P<remind_id>\S+)   # the remind_id
    """, re.VERBOSE)
    
    def __init__(self, msg, conf):
        """Build a new AtCmd class"""
        self.msg = msg
        self.conf = conf
        self.fromh = msg['From']
        self.from_addr = email.utils.parseaddr(self.fromh)[1]
        self.subject = msg['Subject']
        try:
            self.mycommand = msg['Delivered-To'].split('@')[0]
        except AttributeError:
            self.mycommand = msg['To'].split('@')[0]
        self.mycommand = self.mycommand.lower()
        
        # re declared here, 'cause it need _ to be installed
        self.timeshift = re.compile(r"""
            ^\+(?:
                 (?:(?P<year>\d+)[y%(y)s])        # years
                |(?:(?P<month>\d+)[m%(m)s])       # months
                |(?:(?P<week>\d+)[w%(w)s])        # weeks
                |(?:(?P<day>\d+)[d%(d)s])         # days
                |(?:(?P<hour>\d+)[h%(h)s:](?P<minute>\d+)?)   # hours and minutes
            )+$
        """ % {
            'y': _('y'),
            'm': _('m'),
            'w': _('w'),
            'd': _('d'),
            'h': _('h'),
        }, re.VERBOSE)
    
    def process(self):
        """Parse command, and call the right action"""
        _print('#Receive a new command: ' + self.mycommand)
        abstimesearch = self.abstime.search(self.mycommand)
        shiftsearch = self.timeshift.search(self.mycommand)
        if 'help' in self.mycommand or _('help') in self.mycommand:
            self._help_cmd()
        elif abstimesearch:
            _print(abstimesearch.groups())
            # build destination time
            desttime = datetime.datetime(*(
                int(val) for val in abstimesearch.groups(0)
            ))
            self._at_cmd(desttime)
        elif shiftsearch:
            # build destination time
            days = 0
            if shiftsearch.group('day'):
                days += int(shiftsearch.group('day'))
            if shiftsearch.group('month'):
                days += int(shiftsearch.group('month'))*30
            if shiftsearch.group('year'):
                days += int(shiftsearch.group('year'))*365
            weeks = (int(shiftsearch.group('week'))
                        if shiftsearch.group('week') else 0)
            hours = (int(shiftsearch.group('hour'))
                        if shiftsearch.group('hour') else 0)
            mins = (int(shiftsearch.group('minute')) 
                        if shiftsearch.group('minute') else 0)
            shift = datetime.timedelta(days=days, minutes=mins, hours=hours,
                                        weeks=weeks)
            desttime = datetime.datetime.now() + shift
            self._at_cmd(desttime)
        elif 'list' in self.mycommand or _('list') in self.mycommand:
            self._atq_cmd()
        elif self.mycommand.startswith('rm'):
            self._atrm_cmd(self.mycommand[2:])
        else:
            info = _("Command <%s@%s> is not valid... here is some help.") \
                        % (self.mycommand, self.conf['service_domain'])
            self._help_cmd(info)
    
    def _at_cmd(self, desttime):
        """Call AT to programm the remind"""
        # Build the remind reply
        _print('|> command is a remind for %s to be sent at ' % self.fromh, desttime) 
        reply = ReplyMessage(self.msg,
            _('Remind set the %s.') % datetime.datetime.now().strftime('%c'),
            subject_prefix=_('Remind:'),
            fromadress=_('help') + '@' + self.conf['service_domain']
        )
        reply['X-Remind-To'] = self.msg['To']
        _print('|> ' + reply.as_string().replace('\n', '\n|> '))
        # Build an uuid and save it
        remind_id = self.from_addr + '-' + str(uuid.uuid4())
        filename = remind_id + '.eml'
        with open(os.path.join(self.conf['spool_dir'], filename), 'w') as replyfile:
            replyfile.write(reply.as_string())
        _print('|> saved in the spool with id=%s' % remind_id)
        # Programm a new at task
        at_cmdline = ['at',  '-t', desttime.strftime('%y%m%d%H%M')]
        at_stdin = '%s -s %s\n' % (sys.argv[0], remind_id)
        _print('|>', at_cmdline)
        _print('|> >', at_stdin)
        at = Popen(at_cmdline, stdin=PIPE)
        at.communicate(input=at_stdin.encode('utf-8'))
        
    def _help_cmd(self, info=None):
        """Return an help message"""
        if info is None:
            info = ''
            message = _(helpmessage) % {
                'service_name': self.conf['service_name'], 
                'service_domain': self.conf['service_domain'],
                'name_line': '*' * len(self.conf['service_name']),
            }
        else:
            message = info + (_(helpmessage) % {
                'service_name': self.conf['service_name'], 
                'service_domain': self.conf['service_domain'],
                'name_line': '*' * len(self.conf['service_name']),
            })

        reply = ReplyMessage(self.msg,
                    message,
                    False,
                    fromadress=_('help') + '@' + self.conf['service_domain'],
                    newsubject=self.conf['service_name'] + ' - ' + _('Help')
        )
        _print('|> command is help, sending help mail')
        _print('|> ' + reply.as_string().replace('\n', '\n|> '))
        reply.send()
    
    def _atq_cmd(self, info=None):
        """Return the list of personnal reminds"""
        _print('|> command is list, for %s' % self.from_addr)
        # call atq to got tuples of (at_id, at_date)
        atq = Popen("atq", stdout=PIPE)
        atq_result = atq.communicate()[0].decode('utf-8')
        
        remind_list = [] # list of (atq_id, atq_date, subject, orig_recipient)
        for atq_id, atq_date in self.atqtask.findall(atq_result):
            # get details on the at task
            atc = Popen(["at", "-c", atq_id], stdout=PIPE)
            atc_result = atc.communicate()[0].decode('utf-8')
            
            # filter to get remind_id only if remind task & owned by the user
            atc_search = self.atc_remid.search(atc_result)
            if atc_search:
                rem_id = atc_search.group('remind_id')
                if rem_id.startswith(self.from_addr + '-'):
                    # open the message to get the subject and the recipient
                    filename = rem_id + '.eml'
                    filepath = os.path.join(self.conf['spool_dir'], filename)
                    # read remind
                    with open(filepath, 'r') as replyfile:
                        remind = email.message_from_file(replyfile)
                    remind_list.append((atq_id, atq_date, remind['Subject'],
                                        remind['X-Remind-To']))
        
        # build the reply
        if info is None:
            info = ''
        else:
            info += '\n\n'
        if not remind_list:
            list_message = _('You have no programmed remind.')
        else:
            list_message = _('Your reminds:') + '\n'
            for (atq_id, atq_date, subject, orig_recipient) in remind_list:
                list_message += (
                    '  - "' + subject + '" - ' + orig_recipient + '\n' +
                    '    ' + atq_date + ' <> ' + 
                        _('to delete it, send an e-mail to:') + 
                        (' <rm%s@%s>\n\n' % (atq_id, self.conf['service_domain']))
                )
        reply = ReplyMessage(self.msg,
            info + list_message,
            False,
            fromadress=_('help') + '@' + self.conf['service_domain'],
            newsubject=self.conf['service_name'] + ' - ' + _('My reminds')
        )
        reply.send()
    
    def _atrm_cmd(self, atq_id):
        """Remove a remind, defined by it's atq_id"""
        # check if atq_id exists, and if the user as rights on it
        # get details on the at task
        atc = Popen(["at", "-c", atq_id], stdout=PIPE)
        atc_result = atc.communicate()[0].decode('utf-8')
        atc_search = self.atc_remid.search(atc_result)
        if atc_search:
            rem_id = atc_search.group('remind_id')
            if rem_id.startswith(self.from_addr + '-'):
                # remove the task in at and in the spool
                check_call(['atrm', atq_id])
                filename = rem_id + '.eml'
                filepath = os.path.join(self.conf['spool_dir'], filename)
                os.remove(filepath)
                self._atq_cmd(_('The remind %s was succesfully deleted.') %
                                atq_id)
                return
        # else
        self._atq_cmd(_('The remind %s does not exists.') % atq_id)

def send_remind(remind_id, conf):
    """Read the remind mail from the disk, send it, and delete file"""
    filename = remind_id + '.eml'
    filepath = os.path.join(conf['spool_dir'], filename)
    # read reply
    with open(filepath, 'r') as replyfile:
        remind = email.message_from_file(replyfile)
    # send it
    ReplyMessage.send(remind)
    # delete the file
    os.remove(filepath)

if __name__ == '__main__':
    # parsing command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--logfile", type=str,
                    help="place to log action (default is stdout)")
    parser.add_argument("-s", "--send", type=str, metavar="MSG_ID",
                    help="Send the remind mail identified by MSG_ID.")
    parser.add_argument("-c", "--config", type=str, metavar="CONF_FILE",
                    default="/etc/remindat.conf",
                    help="location of the configuration file")
    parser.add_argument("--localedir", type=str, default=None,
                    help="localisation directory")
    args = parser.parse_args()
    # Manage logging stuff
    if args.logfile:
        def _print(*msg):
            with open(args.logfile, 'a') as logf:
                print(*msg, file=logf)
    
    # Load configuration
    _print("Config file:", args.config)
    config = configparser.ConfigParser()
    config.read(args.config)    
    try:
        conf = {
            'service_name': config['Service']['name'],
            'service_domain': config['Service']['domain'],
            'spool_dir': config['Spool'].get('directory', "/var/spool/remindat"),
            'language': config['Service'].get('language', 'en')
        }
    except KeyError as kerror:
        _print("Error: Config file not found, or key '%s' missing. Exiting." % 
                kerror.args[0]) 
        sys.exit(1)
    
    # Load a translation
    _print("Locale:", conf['language'], "from", args.localedir)
    lang = gettext.translation('remindat', args.localedir, 
                                languages=[conf['language']])
    lang.install()
    
    if args.send:
        # Send the remind mail
        send_remind(args.send, conf)
    else:
        # get an e-mail on stdin, process it
        atcmd = AtCmd(email.message_from_file(sys.stdin), conf)
        atcmd.process()
    
